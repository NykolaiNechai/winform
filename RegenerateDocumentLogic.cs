﻿using Autofac;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Veo.AgreementProxy.Abstract;
using Veo.BusinessLogic.Restructuring.Abstract;
using Veo.Data.Infrastructure.Default.Abstract.Repository;
using Veo.Model.Entities;
using Veo.Model.Entities.Restructuring;
using Veo.Model.Enums;
using Veo.Service.Files.Abstract;
using Veo.Storage.Client.Abstractions;

namespace DocumentTool.RegenerateDocuments
{
    public class RegenerateDocumentLogic : IDisposable
    {
        IRepository<Application> _applicationRepository;
        IRepository<LoanProlongation> _loanProlongationReposotory;
        IRepository<RestructuringRequest> _restructuringRequestRepository;
        IRestructuringPlanCalculator _restructuringPlanCalculator;
        IRepository<LoanTransaction> _loanTransactionRepisotory;
        IAgreementWrapper _agreementWrapper;
        ILifetimeScope _scope;
        IStorageClient _storageClient;
        IDocumentService _documentService;

        int? _appId;
        bool _isAgreement, _isRestructuring, _isProlongations, _isTopup, _onlyForOneApp;
        DateTime _edgeDateFrom, _edgeDateTo;

        string _currentProcessName;
        string CurrentProcessName
        {
            get {
                return _currentProcessName;
            }
            set {
                _currentProcessName = value;
                _takeProcessTypeEvent.Invoke(this, new RegenerateDocumentLogicEventArgs<string> { Value = _currentProcessName });
            }
        }

        int _quantity;
        int Quantity
        {
            get {
                return _quantity;
            }
            set {
                _quantity = value;
                _quantityEvent.Invoke(this, new RegenerateDocumentLogicEventArgs<int> { Value = _quantity });
            }
        }

        int _processedQuantity;
        int ProcessedQuantity
        {
            get {
                return _processedQuantity;
            }
            set {
                _processedQuantity = value;
                _processedEvent.Invoke(this, new RegenerateDocumentLogicEventArgs<int> { Value = _processedQuantity });
            }
        }

        EventHandler _takeAppEvent = null;
        EventHandler _takeProcessTypeEvent = null;
        EventHandler _processedEvent = null;
        EventHandler _quantityEvent = null;
        EventHandler _initEvent = null;

        public event EventHandler TakeAppEvent
        {
            add { _takeAppEvent += value; }
            remove { _takeAppEvent -= value; }
        }

        public event EventHandler TakeProcessTypeEvent
        {
            add { _takeProcessTypeEvent += value; }
            remove { _takeProcessTypeEvent -= value; }
        }

        public event EventHandler ProcessedEvent
        {
            add { _processedEvent += value; }
            remove { _processedEvent -= value; }
        }

        public event EventHandler QuantityEvent
        {
            add { _quantityEvent += value; }
            remove { _quantityEvent -= value; }
        }

        public event EventHandler InitEvent
        {
            add { _initEvent += value; }
            remove { _initEvent -= value; }
        }

        public RegenerateDocumentLogic(
            bool isAgreement,
            bool isRestructuring,
            bool isProlongations,
            bool isTopup,
            bool onlyForOneApp,
            DateTime edgeDateFrom,
            DateTime edgeDateTo,
            int appId) {
            _isAgreement = isAgreement;
            _isRestructuring = isRestructuring;
            _isProlongations = isProlongations;
            _isTopup = isTopup;
            _onlyForOneApp = onlyForOneApp;
            _edgeDateFrom = edgeDateFrom;
            _edgeDateTo = edgeDateTo;
            _appId = appId;
        }

        public void Dispose() {
            _scope.Dispose();
        }

        void Initialization() {
            _scope = DependencyResolver.BeginLifetimeScope();
            _applicationRepository = _scope.Resolve<IRepository<Application>>();
            _loanProlongationReposotory = _scope.Resolve<IRepository<LoanProlongation>>();
            _restructuringRequestRepository = _scope.Resolve<IRepository<RestructuringRequest>>();
            _restructuringPlanCalculator = _scope.Resolve<IRestructuringPlanCalculator>();
            _loanTransactionRepisotory = _scope.Resolve<IRepository<LoanTransaction>>();
            _agreementWrapper = _scope.Resolve<IAgreementWrapper>();
            _storageClient = _scope.Resolve<IStorageClient>();
            _documentService = _scope.Resolve<IDocumentService>();

            _initEvent.Invoke(this, new RegenerateDocumentLogicEventArgs<bool> { Value = true });
        }

        public async Task ExecuteAsync() {
            await Task.Run(() => Execute());
        }

        public void Execute() {
            if (_scope == null) {
                Initialization();
            }

            if (_isAgreement) {
                DoAgreement();
            }
            if (_isRestructuring) {
                DoRestructuring();
            }

            if (_isProlongations) {
                DoProlongation();
            }

            if (_isTopup) {
                DoTopUp();
            }
        }

        private void DoAgreement() {
            CurrentProcessName = "Agreement";
            if (_isAgreement == true) {
                IQueryable<Application> applications;
                if (_onlyForOneApp && _appId.HasValue) {
                    applications = _applicationRepository.GetAll()
                        .Where(s => s.Created >= _edgeDateFrom && s.Created <= _edgeDateTo &&
                        s.Id == _appId);
                }
                else {
                    applications = _applicationRepository.GetAll()
                        .Where(s => s.Created >= _edgeDateFrom && s.Created <= _edgeDateTo);
                }

                Quantity = applications.Count();
                ProcessedQuantity = 0;

                foreach (Application application in applications) {
                    Application application1 = application;
                    _takeAppEvent.Invoke(this, new RegenerateDocumentLogicEventArgs<int> { Value = application1.Id });

                    ExecuteAction(() => {
                        if (_agreementWrapper.AgreementExist(application1, true)) {
                            var agreementPath = _documentService.GetAgreementPath(application1.Id);
                            CreateBackUpIfExistsFile(application1.Id, agreementPath, checkPng: true);

                            _agreementWrapper.GenerateAgreement(application1);
                        }
                        else {
                            LogNotFoundFile(application1.Id, CurrentProcessName);
                        }
                    }, application.User.Id, application.Id);
                    ProcessedQuantity++;
                }
            }
        }

        private void DoRestructuring() {
            CurrentProcessName = "Restructuring1";
            if (_isRestructuring) {
                RestructuringRequest[] restructuringRequests;
                if (_onlyForOneApp) {
                    restructuringRequests =
                    _restructuringRequestRepository.GetAll()
                        .Where(s => s.Created >= _edgeDateFrom && s.Created <= _edgeDateTo &&
                        s.RestructuringPlans.Any(x => x.IsActive) &&
                        s.RestructuringPlans.Any(x => x.Application.Id == _appId))
                        .ToArray();
                }
                else {
                    restructuringRequests =
                    _restructuringRequestRepository.GetAll()
                        .Where(s => s.Created >= _edgeDateFrom && s.Created <= _edgeDateTo &&
                        s.RestructuringPlans.Any(x => x.IsActive))
                        .ToArray();
                }

                Quantity = restructuringRequests.Count();
                ProcessedQuantity = 0;
                foreach (RestructuringRequest restReq in restructuringRequests) {
                    var restructuringPlan = restReq.RestructuringPlans.FirstOrDefault();
                    if (restructuringPlan == null) {
                        continue;
                    }

                    var app = restructuringPlan.Application;
                    _takeAppEvent.Invoke(this, new RegenerateDocumentLogicEventArgs<int> { Value = app.Id });
                    RestructuringRequest req = restReq;
                    ExecuteAction(() => {
                        if (_agreementWrapper.RestructuringAgreementExists(app, true)) {
                            var fileRestrucPath = _documentService.GetRestructuringPath(restructuringPlan.Id);
                            CreateBackUpIfExistsFile(app.Id, fileRestrucPath, checkPng: true);

                            _agreementWrapper.GenerateRestructuring(app, req);
                        }
                        else {
                            LogNotFoundFile(app.Id, CurrentProcessName);
                        }
                    }, app.User.Id, app.Id);
                    ProcessedQuantity++;
                }

                Application[] restructuringRequestsApplications;
                if (_onlyForOneApp) {
                    restructuringRequestsApplications =
                    _applicationRepository.GetAll()
                        .Where(x => x.RestructuringRequest != null &&
                        x.RestructuringRequest.Created >= _edgeDateFrom &&
                        x.RestructuringRequest.Created <= _edgeDateTo &&
                        x.Id == _appId)
                        .ToArray();
                }
                else {
                    restructuringRequestsApplications =
                        _applicationRepository.GetAll()
                        .Where(x => x.RestructuringRequest != null && x.RestructuringRequest.Created >= _edgeDateFrom &&
                        x.RestructuringRequest.Created <= _edgeDateTo)
                        .ToArray();
                }

                CurrentProcessName = "Restructuring2";
                Quantity = restructuringRequestsApplications.Count();
                ProcessedQuantity = 0;

                foreach (Application restrReqApp in restructuringRequestsApplications) {
                    Application app = restrReqApp;
                    RestructuringRequest restrReq = restrReqApp.RestructuringRequest;

                    _takeAppEvent.Invoke(this, new RegenerateDocumentLogicEventArgs<int> { Value = app.Id });
                    ExecuteAction(() => {
                        if (_agreementWrapper.RestructuringAgreementExists(app, true)) {

                            var firstRestructuring = app.RestructuringPlans.OrderByDescending(m => m.Id).FirstOrDefault();
                            if (firstRestructuring == null || firstRestructuring.RestructuringRequest == null) {
                                return;
                            }
                            var fileRestrucPath = _documentService.GetRestructuringPath(firstRestructuring.Id);
                            CreateBackUpIfExistsFile(app.Id, fileRestrucPath, checkPng: true);

                            List<RestructuringPlan> plans = _restructuringPlanCalculator.CalculatePlan(restrReq.Restructuring, app).ToList();
                            app.RestructuringPlans = plans;
                            _agreementWrapper.GenerateRestructuring(app, app.RestructuringRequest);
                        }
                        else {
                            LogNotFoundFile(app.Id, CurrentProcessName);
                        }
                    }, app.User.Id, app.Id);
                    ProcessedQuantity++;
                }
            }
        }

        private void DoProlongation() {
            CurrentProcessName = "Prolongation";

            if (_isProlongations) {
                List<LoanProlongation> prolongations;
                if (_onlyForOneApp == true) {
                    prolongations = _loanProlongationReposotory.GetAll()
                        .Where(s => s.Created >= _edgeDateFrom && s.Created <= _edgeDateTo &&
                        s.Application.Id == _appId)
                        .ToList();
                }
                else {
                    prolongations = _loanProlongationReposotory.GetAll()
                        .Where(s => s.Created >= _edgeDateFrom && s.Created <= _edgeDateTo)
                        .ToList();
                }

                Quantity = prolongations.Count();
                ProcessedQuantity = 0;

                foreach (LoanProlongation loanProlongation in prolongations) {
                    LoanProlongation prolongation = loanProlongation;
                    _takeAppEvent.Invoke(this, new RegenerateDocumentLogicEventArgs<int> { Value = prolongation.Application.Id });

                    ExecuteAction(() => {
                        if (_agreementWrapper.ProlongationAgreementExists(prolongation.Application, true)) {
                            var fileProlongationPath = _documentService.GetProlongationPath(loanProlongation.Id);
                            CreateBackUpIfExistsFile(loanProlongation.Application.Id, fileProlongationPath, checkPng: true);

                            _agreementWrapper.GenerateProlongation(prolongation);
                        }
                        else {
                            LogNotFoundFile(loanProlongation.Application.Id, CurrentProcessName);
                        }
                    }, loanProlongation.Application.User.Id, loanProlongation.Application.Id);
                    ProcessedQuantity++;
                }
            }
        }

        private void DoTopUp() {
            CurrentProcessName = "TopUp";
            if (_isTopup == true) {
                IQueryable<LoanTransaction> topUpTransactions;
                if (_onlyForOneApp) {
                    topUpTransactions = _loanTransactionRepisotory.GetAll()
                        .Where(t => t.Date >= _edgeDateFrom && t.Date <= _edgeDateTo &&
                        t.TransactionType.Id == (int)TransactionTypeEnum.Out &&
                        t.Context == TransactionContext.TopUp &&
                        t.Application.Id == _appId.Value);
                }
                else {
                    topUpTransactions = _loanTransactionRepisotory.GetAll()
                    .Where(t => t.Date >= _edgeDateFrom && t.Date <= _edgeDateTo &&
                                t.TransactionType.Id == (int)TransactionTypeEnum.Out &&
                                t.Context == TransactionContext.TopUp);
                }

                Quantity = topUpTransactions.Select(x => x.Application.Id).Distinct().Count();
                ProcessedQuantity = 0;

                foreach (var topUpTransaction in topUpTransactions) {
                    if (topUpTransaction == null) {
                        continue;
                    }
                    LoanTransaction transaction = topUpTransaction;
                    _takeAppEvent.Invoke(this, new RegenerateDocumentLogicEventArgs<int> { Value = transaction.Application.Id });

                    ExecuteAction(() => {
                        if (_agreementWrapper.TopUpAgreementExists(transaction.Application, transaction.Id, true)) {

                            var fileTopUpPath = _documentService.GetTopUpAgreementPath(transaction.Application.Id, transaction.Id);
                            CreateBackUpIfExistsFile(transaction.Application.Id, fileTopUpPath, checkPng: true);

                            _agreementWrapper.GenerateTopUpAgreement(transaction.Application, transaction);
                        }
                        else {
                            LogNotFoundFile(transaction.Application.Id, CurrentProcessName);
                        }
                    }, transaction.Application.User.Id, transaction.Application.Id);
                    ProcessedQuantity++;
                }
            }
        }

        private void ExecuteAction(Action action, int? userId, int? appId) {
            try {
                action();
            }
            catch (Exception ex) {
                var ee = ex.Message;
            }
        }

        private void CreateBackUpIfExistsFile(int appId, string filePath, bool checkPng) {
            var isExists = _storageClient.IsExists(filePath) && (!checkPng || _storageClient.IsExists(_documentService.GetAgreementPreviewPath(appId)));

            if (isExists) {
                var backUpFilePath = string.Format("{0}.{1}.backup.pdf", filePath, DateTime.Now.Ticks);

                CreateBackUp(filePath, backUpFilePath);
                LogInfoToFile(backUpFilePath, filePath, appId);
            }
        }

        private void CreateBackUp(string filePath, string backUpFilePath) {
            using (var targetStream = new MemoryStream()) {
                _storageClient.DownloadToStream(filePath, targetStream);

                targetStream.Position = 0;
                _storageClient.UploadFromStream(backUpFilePath, targetStream);
            }
        }

        private void LogInfoToFile(string backUpPath, string realPath, int appid) {
            string path = @"c:\DocumentServiceClient_Report.txt";

            if (!File.Exists(path)) {
                using (StreamWriter sw1 = File.CreateText(path)) { }
            }

            StreamWriter sw = File.AppendText(path);
            sw.WriteLine(string.Format("[{3}] [AppId:{0}]; \n oldFile:{1}; \n newFile:{2}", appid, backUpPath, realPath, DateTime.Now));
            sw.WriteLine();
            sw.Close();
        }

        private void LogNotFoundFile(int appid, string type) {
            string path = @"c:\DocumentServiceClient_NotFound_Report.txt";

            if (!File.Exists(path)) {
                using (StreamWriter sw1 = File.CreateText(path)) { }
            }

            StreamWriter sw = File.AppendText(path);
            sw.WriteLine(string.Format("[{0}] [AppId:{1}]; type:{2};", DateTime.Now, appid, type));
            sw.WriteLine();
            sw.Close();
        }
    }
}
