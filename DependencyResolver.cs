﻿using Autofac;
using Veo.AutofacModules;
using Veo.AutofacModules.Session;

namespace DocumentTool.RegenerateDocuments
{
    public class DependencyResolver
    {
        private static IContainer _container;

        private static IContainer Container
        {
            get {
                if (_container == null) {
                    var builder = new ContainerBuilder();
                    builder.RegisterModule(new ServiceCacheAutofacModule());
                    builder.RegisterModule(new SessionDefaultAutofacModule());
                    builder.RegisterModule(new DefaultVeoAutofacModule());

                    //builder.RegisterStorage();

                    _container = builder.Build();
                }

                return _container;
            }
        }

        public static ILifetimeScope BeginLifetimeScope() {
            return Container.BeginLifetimeScope();
        }

        public static T GetObject<T>() {
            return Container.Resolve<T>();
        }
    }
}
