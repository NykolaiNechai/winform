﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentTool.RegenerateDocuments
{
    public class RegenerateDocumentLogicService
    {
        Form1 _formDocumentToolRegenerateDocuments = null;
        RegenerateDocumentLogic _regenerateDocumentLogic = null;

        int _countOfProcessedRows;
        int _quantityRows;

        public RegenerateDocumentLogicService(Form1 form) {
            _formDocumentToolRegenerateDocuments = form;
        }

        public void Execute() {
            _regenerateDocumentLogic.Execute();
        }

        public async Task ExecuteAsync() {
            Init();
            await _regenerateDocumentLogic.ExecuteAsync();
            _regenerateDocumentLogic.Dispose();

            DisableOrEnableControls(false);
        }

        private void Init() {
            DisableOrEnableControls(true);

            _regenerateDocumentLogic = new RegenerateDocumentLogic(
                _formDocumentToolRegenerateDocuments.chbAgreement.Checked,
                _formDocumentToolRegenerateDocuments.chbRestructuring.Checked,
                _formDocumentToolRegenerateDocuments.chbProlongation.Checked,
                _formDocumentToolRegenerateDocuments.chbTopUp.Checked,
                _formDocumentToolRegenerateDocuments.chbOnlyForAppId.Checked,
                _formDocumentToolRegenerateDocuments.dtpHead.Value,
                _formDocumentToolRegenerateDocuments.dtpTail.Value,
                Convert.ToInt32(_formDocumentToolRegenerateDocuments.tbAppId.Text));

            Subscribe();
        }

        private void DisableOrEnableControls(bool disable) {
            if (disable) {
                _formDocumentToolRegenerateDocuments.pnPeriod.DisableControls<DateTimePicker>();
                _formDocumentToolRegenerateDocuments.pnDocType.DisableControls<CheckBox>();
                _formDocumentToolRegenerateDocuments.pnDocType.DisableControls<TextBox>();
                _formDocumentToolRegenerateDocuments.DisableControls<Button>();
            }
            else {
                _formDocumentToolRegenerateDocuments.pnPeriod.EnableControls<DateTimePicker>();
                _formDocumentToolRegenerateDocuments.pnDocType.EnableControls<CheckBox>();
                _formDocumentToolRegenerateDocuments.pnDocType.EnableControls<TextBox>();
                _formDocumentToolRegenerateDocuments.EnableControls<Button>();
            }
        }

        private void Subscribe() {
            _regenerateDocumentLogic.TakeAppEvent += TakeAppEventHandler;
            _regenerateDocumentLogic.TakeProcessTypeEvent += TakeProcessTypeEventHandler;
            _regenerateDocumentLogic.ProcessedEvent += ProcessedEventHandler;
            _regenerateDocumentLogic.QuantityEvent += QuantityEventHandler;
            _regenerateDocumentLogic.InitEvent += InitEventHandler;
        }

        private void TakeAppEventHandler(object sender, EventArgs e) {
            var ea = e as RegenerateDocumentLogicEventArgs<int>;
            var lCurrentAppId = _formDocumentToolRegenerateDocuments.lCurrentAppId;
            if (lCurrentAppId.InvokeRequired) {
                lCurrentAppId.Invoke(new Action(() => {
                    _formDocumentToolRegenerateDocuments.lCurrentAppId.Text = ea.Value.ToString();
                }));
            }
        }

        private void TakeProcessTypeEventHandler(object sender, EventArgs e) {
            var ea = e as RegenerateDocumentLogicEventArgs<string>;
            var lProcessName = _formDocumentToolRegenerateDocuments.lProcessName;
            if (lProcessName.InvokeRequired) {
                lProcessName.Invoke(new Action(() => {
                    _formDocumentToolRegenerateDocuments.lProcessName.Text = ea.Value;
                }));
            }

            var progressBar1 = _formDocumentToolRegenerateDocuments.progressBar1;
            if (progressBar1.InvokeRequired) {
                progressBar1.Invoke(new Action(() => {
                    progressBar1.Value = 0;
                }));
            }
        }

        private void ProcessedEventHandler(object sender, EventArgs e) {
            var ea = e as RegenerateDocumentLogicEventArgs<int>;
            _countOfProcessedRows = ea.Value;

            var left = _quantityRows - _countOfProcessedRows;
            var lProcessed = _formDocumentToolRegenerateDocuments.lProcessed;
            if (lProcessed.InvokeRequired) {
                lProcessed.Invoke(new Action(() => {
                    _formDocumentToolRegenerateDocuments.lProcessed.Text = ea.Value.ToString();
                    _formDocumentToolRegenerateDocuments.lLeft.Text = left.ToString();
                }));
            }

            var progressBar1 = _formDocumentToolRegenerateDocuments.progressBar1;
            if (progressBar1.InvokeRequired) {
                progressBar1.Invoke(new Action(() => {
                    var pross = Math.Ceiling((100.0 / (_quantityRows == 0 ? 1.0 : _quantityRows)) * _countOfProcessedRows);
                    progressBar1.Value = pross >= 100 ? 100 : (int)pross;
                }));
            }
        }

        private void QuantityEventHandler(object sender, EventArgs e) {
            var ea = e as RegenerateDocumentLogicEventArgs<int>;
            _quantityRows = ea.Value;

            var lQuantity = _formDocumentToolRegenerateDocuments.lQuantity;
            if (lQuantity.InvokeRequired) {
                lQuantity.Invoke(new Action(() => {
                    lQuantity.Text = _quantityRows.ToString();
                }));
            }
        }

        private void InitEventHandler(object sender, EventArgs e) {
            var ea = e as RegenerateDocumentLogicEventArgs<bool>;

            var pbLoader = _formDocumentToolRegenerateDocuments.pbLoader;

            if (pbLoader.InvokeRequired) {
                pbLoader.Invoke(new Action(() => {
                    pbLoader.SendToBack();
                    pbLoader.Visible = false;
                }));
            }
        }
    }
}
