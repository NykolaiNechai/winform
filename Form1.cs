﻿using System;
using System.Windows.Forms;

namespace DocumentTool.RegenerateDocuments
{
    public partial class Form1 : Form
    {
        FormUiElementsConfigurator _formUiElementsConfigurator;
        RegenerateDocumentLogicService _regenerateDocumentLogicService;

        public Form1() {
            InitializeComponent();

            _formUiElementsConfigurator = new FormUiElementsConfigurator(this);
            _regenerateDocumentLogicService = new RegenerateDocumentLogicService(this);
        }

        private async void button1_Click(object sender, EventArgs e) {
            pbLoader.BringToFront();
            pbLoader.Visible = true;

            await _regenerateDocumentLogicService.ExecuteAsync();
        }
    }
}
