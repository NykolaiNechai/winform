﻿using System;
using System.Windows.Forms;

namespace DocumentTool.RegenerateDocuments
{
    public class FormUiElementsConfigurator
    {
        Form1 _formDocumentToolRegenerateDocuments = null;

        public FormUiElementsConfigurator(Form1 form) {
            _formDocumentToolRegenerateDocuments = form;

            ConfigureDataTimePickers();
        }

        private void ConfigureDataTimePickers() {
            var dtpHead = _formDocumentToolRegenerateDocuments.dtpHead;
            var dtpTail = _formDocumentToolRegenerateDocuments.dtpTail;

            dtpHead.Format = DateTimePickerFormat.Custom;
            dtpHead.CustomFormat = "d.MM.yyyy HH:mm";
            dtpTail.Format = DateTimePickerFormat.Custom;
            dtpTail.CustomFormat = "d.MM.yyyy HH:mm";

            dtpHead.Value = new DateTime(2019, 01, 09, 17, 25, 59); //new DateTime(2019, 01, 10, 21, 59, 59);
            dtpTail.Value = new DateTime(2019, 01, 11, 00, 00, 20);//new DateTime(2019, 01, 11, 14, 01, 00);

            var chbOnlyForAppId = _formDocumentToolRegenerateDocuments.chbOnlyForAppId;
            var tbAppId = _formDocumentToolRegenerateDocuments.tbAppId;
            chbOnlyForAppId.Checked = true;
            tbAppId.Text = "0";

            var pbLoader = _formDocumentToolRegenerateDocuments.pbLoader;
            pbLoader.Visible = false;
        }
    }
}
