﻿using System;

namespace DocumentTool.RegenerateDocuments
{
    public class RegenerateDocumentLogicEventArgs<T> : EventArgs
    {
        public T Value { get; set; }
    }
}
