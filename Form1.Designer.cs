﻿namespace DocumentTool.RegenerateDocuments
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pnPeriod = new System.Windows.Forms.Panel();
            this.lTail = new System.Windows.Forms.Label();
            this.lHead = new System.Windows.Forms.Label();
            this.dtpTail = new System.Windows.Forms.DateTimePicker();
            this.dtpHead = new System.Windows.Forms.DateTimePicker();
            this.chbAgreement = new System.Windows.Forms.CheckBox();
            this.pnDocType = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tbAppId = new System.Windows.Forms.TextBox();
            this.chbOnlyForAppId = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chbTopUp = new System.Windows.Forms.CheckBox();
            this.chbProlongation = new System.Windows.Forms.CheckBox();
            this.chbRestructuring = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label3 = new System.Windows.Forms.Label();
            this.pnProcess = new System.Windows.Forms.Panel();
            this.pnQuantity = new System.Windows.Forms.Panel();
            this.lProcessed = new System.Windows.Forms.Label();
            this.lLeft = new System.Windows.Forms.Label();
            this.lQuantity = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pnProcessInfo = new System.Windows.Forms.Panel();
            this.lProcessName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnAppInfo = new System.Windows.Forms.Panel();
            this.lCurrentAppId = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pbLoader = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pnPeriod.SuspendLayout();
            this.pnDocType.SuspendLayout();
            this.pnProcess.SuspendLayout();
            this.pnQuantity.SuspendLayout();
            this.pnProcessInfo.SuspendLayout();
            this.pnAppInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoader)).BeginInit();
            this.SuspendLayout();
            // 
            // pnPeriod
            // 
            this.pnPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnPeriod.Controls.Add(this.lTail);
            this.pnPeriod.Controls.Add(this.lHead);
            this.pnPeriod.Controls.Add(this.dtpTail);
            this.pnPeriod.Controls.Add(this.dtpHead);
            this.pnPeriod.Location = new System.Drawing.Point(12, 12);
            this.pnPeriod.Name = "pnPeriod";
            this.pnPeriod.Size = new System.Drawing.Size(258, 71);
            this.pnPeriod.TabIndex = 0;
            // 
            // lTail
            // 
            this.lTail.AutoSize = true;
            this.lTail.Location = new System.Drawing.Point(210, 47);
            this.lTail.Name = "lTail";
            this.lTail.Size = new System.Drawing.Size(24, 13);
            this.lTail.TabIndex = 3;
            this.lTail.Text = "Tail";
            // 
            // lHead
            // 
            this.lHead.AutoSize = true;
            this.lHead.Location = new System.Drawing.Point(210, 11);
            this.lHead.Name = "lHead";
            this.lHead.Size = new System.Drawing.Size(33, 13);
            this.lHead.TabIndex = 2;
            this.lHead.Text = "Head";
            // 
            // dtpTail
            // 
            this.dtpTail.Location = new System.Drawing.Point(4, 40);
            this.dtpTail.Name = "dtpTail";
            this.dtpTail.Size = new System.Drawing.Size(200, 20);
            this.dtpTail.TabIndex = 1;
            // 
            // dtpHead
            // 
            this.dtpHead.Location = new System.Drawing.Point(4, 4);
            this.dtpHead.Name = "dtpHead";
            this.dtpHead.Size = new System.Drawing.Size(200, 20);
            this.dtpHead.TabIndex = 0;
            // 
            // chbAgreement
            // 
            this.chbAgreement.AutoSize = true;
            this.chbAgreement.Checked = true;
            this.chbAgreement.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbAgreement.Location = new System.Drawing.Point(3, 9);
            this.chbAgreement.Name = "chbAgreement";
            this.chbAgreement.Size = new System.Drawing.Size(77, 17);
            this.chbAgreement.TabIndex = 4;
            this.chbAgreement.Text = "Agreement";
            this.chbAgreement.UseVisualStyleBackColor = true;
            // 
            // pnDocType
            // 
            this.pnDocType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnDocType.Controls.Add(this.label2);
            this.pnDocType.Controls.Add(this.tbAppId);
            this.pnDocType.Controls.Add(this.chbOnlyForAppId);
            this.pnDocType.Controls.Add(this.label1);
            this.pnDocType.Controls.Add(this.chbTopUp);
            this.pnDocType.Controls.Add(this.chbProlongation);
            this.pnDocType.Controls.Add(this.chbRestructuring);
            this.pnDocType.Controls.Add(this.chbAgreement);
            this.pnDocType.Location = new System.Drawing.Point(276, 12);
            this.pnDocType.Name = "pnDocType";
            this.pnDocType.Size = new System.Drawing.Size(332, 106);
            this.pnDocType.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(109, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "AppId";
            // 
            // tbAppId
            // 
            this.tbAppId.Location = new System.Drawing.Point(3, 77);
            this.tbAppId.Name = "tbAppId";
            this.tbAppId.Size = new System.Drawing.Size(100, 20);
            this.tbAppId.TabIndex = 10;
            this.tbAppId.Text = "0";
            // 
            // chbOnlyForAppId
            // 
            this.chbOnlyForAppId.AutoSize = true;
            this.chbOnlyForAppId.Checked = true;
            this.chbOnlyForAppId.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbOnlyForAppId.Location = new System.Drawing.Point(3, 54);
            this.chbOnlyForAppId.Name = "chbOnlyForAppId";
            this.chbOnlyForAppId.Size = new System.Drawing.Size(88, 17);
            this.chbOnlyForAppId.TabIndex = 9;
            this.chbOnlyForAppId.Text = "onlyForAppId";
            this.chbOnlyForAppId.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(3, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(326, 10);
            this.label1.TabIndex = 8;
            // 
            // chbTopUp
            // 
            this.chbTopUp.AutoSize = true;
            this.chbTopUp.Location = new System.Drawing.Point(272, 9);
            this.chbTopUp.Name = "chbTopUp";
            this.chbTopUp.Size = new System.Drawing.Size(59, 17);
            this.chbTopUp.TabIndex = 7;
            this.chbTopUp.Text = "TopUp";
            this.chbTopUp.UseVisualStyleBackColor = true;
            // 
            // chbProlongation
            // 
            this.chbProlongation.AutoSize = true;
            this.chbProlongation.Location = new System.Drawing.Point(181, 9);
            this.chbProlongation.Name = "chbProlongation";
            this.chbProlongation.Size = new System.Drawing.Size(85, 17);
            this.chbProlongation.TabIndex = 6;
            this.chbProlongation.Text = "Prolongation";
            this.chbProlongation.UseVisualStyleBackColor = true;
            // 
            // chbRestructuring
            // 
            this.chbRestructuring.AutoSize = true;
            this.chbRestructuring.Location = new System.Drawing.Point(86, 9);
            this.chbRestructuring.Name = "chbRestructuring";
            this.chbRestructuring.Size = new System.Drawing.Size(89, 17);
            this.chbRestructuring.TabIndex = 5;
            this.chbRestructuring.Text = "Restructuring";
            this.chbRestructuring.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(5, 74);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(584, 23);
            this.progressBar1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Status";
            // 
            // pnProcess
            // 
            this.pnProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnProcess.Controls.Add(this.pbLoader);
            this.pnProcess.Controls.Add(this.pnQuantity);
            this.pnProcess.Controls.Add(this.pnProcessInfo);
            this.pnProcess.Controls.Add(this.pnAppInfo);
            this.pnProcess.Controls.Add(this.progressBar1);
            this.pnProcess.Controls.Add(this.label3);
            this.pnProcess.Location = new System.Drawing.Point(12, 124);
            this.pnProcess.Name = "pnProcess";
            this.pnProcess.Size = new System.Drawing.Size(596, 106);
            this.pnProcess.TabIndex = 12;
            // 
            // pnQuantity
            // 
            this.pnQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnQuantity.Controls.Add(this.lProcessed);
            this.pnQuantity.Controls.Add(this.lLeft);
            this.pnQuantity.Controls.Add(this.lQuantity);
            this.pnQuantity.Controls.Add(this.label7);
            this.pnQuantity.Location = new System.Drawing.Point(390, 3);
            this.pnQuantity.Name = "pnQuantity";
            this.pnQuantity.Size = new System.Drawing.Size(199, 54);
            this.pnQuantity.TabIndex = 16;
            // 
            // lProcessed
            // 
            this.lProcessed.AutoSize = true;
            this.lProcessed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lProcessed.Location = new System.Drawing.Point(132, 32);
            this.lProcessed.Name = "lProcessed";
            this.lProcessed.Size = new System.Drawing.Size(66, 13);
            this.lProcessed.TabIndex = 15;
            this.lProcessed.Text = "Processed";
            // 
            // lLeft
            // 
            this.lLeft.AutoSize = true;
            this.lLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lLeft.Location = new System.Drawing.Point(74, 32);
            this.lLeft.Name = "lLeft";
            this.lLeft.Size = new System.Drawing.Size(29, 13);
            this.lLeft.TabIndex = 14;
            this.lLeft.Text = "Left";
            // 
            // lQuantity
            // 
            this.lQuantity.AutoSize = true;
            this.lQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lQuantity.Location = new System.Drawing.Point(3, 32);
            this.lQuantity.Name = "lQuantity";
            this.lQuantity.Size = new System.Drawing.Size(54, 13);
            this.lQuantity.TabIndex = 13;
            this.lQuantity.Text = "Quantity";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(130, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "QuantityInCurrentProcess:";
            // 
            // pnProcessInfo
            // 
            this.pnProcessInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnProcessInfo.Controls.Add(this.lProcessName);
            this.pnProcessInfo.Controls.Add(this.label6);
            this.pnProcessInfo.Location = new System.Drawing.Point(188, 3);
            this.pnProcessInfo.Name = "pnProcessInfo";
            this.pnProcessInfo.Size = new System.Drawing.Size(196, 35);
            this.pnProcessInfo.TabIndex = 15;
            // 
            // lProcessName
            // 
            this.lProcessName.AutoSize = true;
            this.lProcessName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lProcessName.Location = new System.Drawing.Point(91, 9);
            this.lProcessName.Name = "lProcessName";
            this.lProcessName.Size = new System.Drawing.Size(84, 13);
            this.lProcessName.TabIndex = 13;
            this.lProcessName.Text = "ProcessName";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "CurrentProcess:";
            // 
            // pnAppInfo
            // 
            this.pnAppInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnAppInfo.Controls.Add(this.lCurrentAppId);
            this.pnAppInfo.Controls.Add(this.label4);
            this.pnAppInfo.Location = new System.Drawing.Point(6, 3);
            this.pnAppInfo.Name = "pnAppInfo";
            this.pnAppInfo.Size = new System.Drawing.Size(176, 35);
            this.pnAppInfo.TabIndex = 14;
            // 
            // lCurrentAppId
            // 
            this.lCurrentAppId.AutoSize = true;
            this.lCurrentAppId.Location = new System.Drawing.Point(78, 9);
            this.lCurrentAppId.Name = "lCurrentAppId";
            this.lCurrentAppId.Size = new System.Drawing.Size(69, 13);
            this.lCurrentAppId.TabIndex = 13;
            this.lCurrentAppId.Text = "CurrentAppId";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "CurrentAppId:";
            // 
            // pbLoader
            // 
            this.pbLoader.Image = ((System.Drawing.Image)(resources.GetObject("pbLoader.Image")));
            this.pbLoader.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbLoader.InitialImage")));
            this.pbLoader.Location = new System.Drawing.Point(-11, -4);
            this.pbLoader.Name = "pbLoader";
            this.pbLoader.Size = new System.Drawing.Size(614, 122);
            this.pbLoader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbLoader.TabIndex = 14;
            this.pbLoader.TabStop = false;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(16, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(254, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Start-Regeneration";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 240);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pnProcess);
            this.Controls.Add(this.pnDocType);
            this.Controls.Add(this.pnPeriod);
            this.Name = "Form1";
            this.Text = "DocumentTool - RegenerateDocuments";
            this.pnPeriod.ResumeLayout(false);
            this.pnPeriod.PerformLayout();
            this.pnDocType.ResumeLayout(false);
            this.pnDocType.PerformLayout();
            this.pnProcess.ResumeLayout(false);
            this.pnProcess.PerformLayout();
            this.pnQuantity.ResumeLayout(false);
            this.pnQuantity.PerformLayout();
            this.pnProcessInfo.ResumeLayout(false);
            this.pnProcessInfo.PerformLayout();
            this.pnAppInfo.ResumeLayout(false);
            this.pnAppInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoader)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnPeriod;
        private System.Windows.Forms.Label lTail;
        private System.Windows.Forms.Label lHead;
        public System.Windows.Forms.DateTimePicker dtpTail;
        public System.Windows.Forms.DateTimePicker dtpHead;
        
        public System.Windows.Forms.Panel pnDocType;
        public System.Windows.Forms.CheckBox chbAgreement;
        public System.Windows.Forms.CheckBox chbProlongation;
        public System.Windows.Forms.CheckBox chbRestructuring;
        public System.Windows.Forms.CheckBox chbTopUp;
        public System.Windows.Forms.TextBox tbAppId;
        private System.Windows.Forms.Label label2;        
        public System.Windows.Forms.CheckBox chbOnlyForAppId;
        private System.Windows.Forms.Label label1;

        public System.Windows.Forms.Panel pnProcess;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label3;

        public System.Windows.Forms.Panel pnProcessInfo;
        public System.Windows.Forms.Label lProcessName;
        private System.Windows.Forms.Label label6;

        public System.Windows.Forms.Panel pnAppInfo;
        public System.Windows.Forms.Label lCurrentAppId;
        private System.Windows.Forms.Label label4;

        public System.Windows.Forms.Panel pnQuantity;
        public System.Windows.Forms.Label lQuantity;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lProcessed;
        public System.Windows.Forms.Label lLeft;

        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.PictureBox pbLoader;
    }
}

